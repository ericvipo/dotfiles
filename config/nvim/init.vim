" Intento de refactorización
"if &compatible | set nocompatible | endif

"runtime viml/options.vim
"runtime viml/plugins.vim
"runtime viml/automations.vim
"runtime viml/commands.vim
"runtime viml/mappings.vim
"
augroup AuBufWritePre
	autocmd!
	autocmd BufWritePre * let current_pos = getpos(".")
	autocmd BufWritePre * silent! undojoin | %s/\s\+$//e
	autocmd BufWritePre * silent! undojoin | %s/\n\+\%$//e
	autocmd BufWritePre * call setpos(".", current_pos)
	autocmd BufWritePre,FileWritePre * silent! call mkdir(expand('<afile>:p:h'), 'p')
augroup END

let mapleader=" "

"let g:minimap_width = 10
"let g:minimap_auto_start = 1
"let g:minimap_auto_start_win_enter = 1


nnoremap <SPACE> <Nop>

map <leader>gy :Goyo<CR>
map <leader>ll :Limelight!!<CR>

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

nnoremap <C-n> :NvimTreeToggle<CR>
nnoremap <leader>r :NvimTreeRefresh<CR>
nnoremap <leader>n :NvimTreeFindFile<CR>

set breakindent
set colorcolumn=81
" guías de indentación
set listchars=tab:\┆\ ,trail:· list "Alternate tab: »>
" Highlight all search matches
set hlsearch
" Ignore uppercase and lowercase when searchs
set ignorecase
" Línea horizontal en el cursor
set cursorline
" Indentar cuando usamos >>
set sw=2
set mouse=a
set number
" Cantidad de espacio al tabular
set tabstop=2
set shiftwidth=2
set wrap
set noshowmode
set termguicolors

syntax on

"noremap <silent> <C-n> :lua require('lua/xpctr/tree').toggle()<CR>

function! s:goyo_enter()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status off
    silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif
  set noshowmode
  set noshowcmd
  set scrolloff=999
  Limelight

  " ...
endfunction

function! s:goyo_leave()
  if executable('tmux') && strlen($TMUX)
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
  set showmode
  set showcmd
  set scrolloff=5
  Limelight!

  " ...
endfunction

autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()


call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')
	Plug 'kyazdani42/nvim-web-devicons' " for file icons
	Plug 'kyazdani42/nvim-tree.lua'
	Plug 'nvim-lua/plenary.nvim'
	Plug 'nvim-telescope/telescope.nvim'
	Plug 'nvim-lualine/lualine.nvim'
	"Plug 'wfxr/minimap.vim'
	Plug 'romgrk/barbar.nvim'
	Plug 'junegunn/goyo.vim'
	Plug 'junegunn/limelight.vim'
	Plug 'Yggdroot/indentline'
	Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
	Plug 'EdenEast/nightfox.nvim', { 'tag': 'v1.0.0' }
call plug#end()

" require plugin configs
lua require('xpctr')

colorscheme nightfox
