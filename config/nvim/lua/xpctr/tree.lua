local tree = {}
tree.toggle = function()
	require'nvim-tree'.toggle()
	if require'nvim-tree.view'.is_visible() then
		require'bufferline.state'.set_offset(31, 'FileTree')
	else
		require'bufferline.state'.set_offset(0)
	end
end

return tree
